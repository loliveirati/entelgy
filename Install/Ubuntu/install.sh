#!/bin/bash
# My first script

sudo apt-add-repository universe

sudo apt-add-repository multiverse

sudo apt-get update

sudo apt-get install default-jdk

wget http://ftp.unicamp.br/pub/apache/tomcat/tomcat-8/v8.0.39/bin/apache-tomcat-8.0.39.tar.gz

tar xvzf apache-tomcat-8.0.39.tar.gz

sudo mv apache-tomcat-8.0.39 /opt/tomcat

/opt/tomcat/bin/startup.sh

cd /home/ubuntu

sudo apt-get install git

git clone https://bitbucket.org/loliveirati/entelgy.git /home/ubuntu/entelgy

cp /home/ubuntu/entelgy/Utils/Tomcat-8/webapps/rest-1.0.war /opt/tomcat/webapps

/opt/tomcat/bin/shutdown.sh

/opt/tomcat/bin/startup.sh

sleep 10

cd /home/ubuntu

cp -R /home/ubuntu/entelgy/Fonte/Frontend/Entegy /opt/tomcat/webapps/rest-1.0/entelgy

firefox http://localhost:8080/rest-1.0/entelgy/index.html
