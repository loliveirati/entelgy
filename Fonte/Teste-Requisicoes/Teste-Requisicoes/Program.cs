﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;

namespace Teste_Requisicoes
{
    class Program
    {
        static void Main(string[] args)
        {
            ExecutarRequisicoes();
            Console.ReadLine();
        }

        private static void ExecutarRequisicoes()
        {
            Console.WriteLine("Quantas requisicoes simultaneas deseja?:");
            int nRequisicoes = int.Parse(Console.ReadLine());

            string endpoint = @"http://localhost:8080/rest-1.0/api/pedido/inserir";
            string jsonPedido = new StreamReader(@"pedido.json").ReadToEnd();

            HttpContent content = new StringContent(jsonPedido, System.Text.Encoding.UTF8, "application/json");

            var client = new RestClient(endpoint);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", jsonPedido, ParameterType.RequestBody);

            Console.WriteLine("IMPORTANTE: Criando pedidos simultaneaos p/ o login: teste@email.com  (senha: 123)");

            for (int i = 1; i < nRequisicoes + 1; i++)
            {
                var result = client.Execute(request);

                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Console.WriteLine("[OK] - Inserido pedido " + i + " de : " + nRequisicoes);
                }
                else
                {
                    Console.WriteLine("[FALHA] - Pedido " + i + " de : " + nRequisicoes);
                }
            }
        }
    }
}
