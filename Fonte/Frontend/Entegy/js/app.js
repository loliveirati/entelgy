﻿var hostService = "http://localhost:8080/rest-1.0/api/"

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

var app = angular.module('MyApp', ['jm.i18next', 'checklist-model']);

app.controller('LoginController', function ($scope, $i18next) {
    $scope.logar = function () {
        if (!$scope.email || $scope.email == "") {
            toastr.error(i18next.t("mensagem.MSG01", { campo: i18next.t("login.lblEmail") }));
            return;
        }

        if (!$scope.senha || $scope.senha == "") {
            toastr.error(i18next.t("mensagem.MSG01", { campo: i18next.t("login.lblPassword") }));
            return;
        }

        //var retorno = $scope.retornarLogin($scope.email, $scope.senha);
        //if (retorno) {
        //    localStorage.setItem("login", JSON.stringify(retorno));
        //    window.location.href = "pedido.html";
        //} else {
        //    toastr.error(i18next.t("mensagem.MSG03"))
        //}

        $scope.chamarLogin($scope.email, $scope.senha);

    }

    $scope.chamarLogin = function (login, senha) {

        urlMetodo = hostService + "login/" + login + "/" + senha;

        $.get(urlMetodo).done(function (data) {
            if (!data) {
                toastr.error(i18next.t("mensagem.MSG03"))
                return;
            }

            retorno = data;
            localStorage.setItem("login", JSON.stringify(retorno));
            window.location.href = "pedido.html";
        }).fail(function (data) {
            toastr.error(i18next.t("mensagem.MSG03"));
        });

    }
});

app.controller('PedidoController', function ($scope, $i18next) {
    var loginStr = window.localStorage.getItem("login");

    if (!loginStr || loginStr == "") {
        window.location.href = "index.html";
    }

    var login = JSON.parse(loginStr);

    

    $scope.atualizarPedidosAnteriores = function () {

        urlMetodo = hostService + "pedido/" + login.email;

        $scope.PedidosAnteriores = []
        $.get(urlMetodo).done(function (data) {
            $scope.PedidosAnteriores = data;
            $scope.$applyAsync();
        })
    }

    $scope.$watch(
      function () { return $scope.lancheProntoSelecionado; },
      function (newValue, oldValue) {
          if (newValue != oldValue) {
              $scope.setarLanche(newValue);
          }
      }
    );

    $scope.$watch(
      function () { return $scope.pedido.quantidadePessoas; },
      function (newValue, oldValue) {
          if (!newValue) newValue = 0;
          if (!oldValue) oldValue = 0;

          if (newValue < 0) newValue = 0;
          if (oldValue < 0) oldValue = 0;

          if (newValue > oldValue) {
              for (var x = 0; x < newValue - oldValue; x++) {
                  var pessoa = {}

                  $scope.pessoas.push(pessoa);

                  $scope.addWatchPessoa(pessoa);
              }
          } else if (newValue < oldValue) {
              for (var x = 0; x < oldValue - newValue; x++) {
                  $scope.pessoas.pop();
              }
          }

          $scope.pedido.quantidadePessoas = newValue;
      }
    );

    $scope.addWatchPessoa = function (pessoa) {
        $scope.$watch(
          function () { return pessoa.valor; },
          function (newValue, oldValue) {
              var totalPedido = $scope.valorPedido();

              if (!newValue) newValue = 0;
              if (!oldValue) oldValue = 0;

              if (newValue < 0) newValue = 0;
              if (newValue > totalPedido) newValue = totalPedido;

              if (newValue != oldValue) {
                  pessoa.porcentagem = (newValue / totalPedido) * 100;
              }

              pessoa.valor = newValue;
          }
        );

        $scope.$watch(
            function () { return pessoa.porcentagem; },
            function (newValue, oldValue) {
                var totalPedido = $scope.valorPedido();

                if (!newValue) newValue = 0;
                if (!oldValue) oldValue = 0;

                if (newValue < 0) newValue = 0;
                if (newValue > 100) newValue = 100;

                if (newValue != oldValue) {
                    pessoa.valor = totalPedido * (newValue / 100);
                }

                pessoa.porcentagem = newValue;
            }
        );
    }

    $scope.atualizarCheck = function (idJquery, checked) {
        setTimeout(function () { $(idJquery).prop("checked", checked); }, 200)
    };

    $scope.retornarNome = function (obj) {
        if (obj) {
            return i18next.t(obj.nome)
        } else {
            return ''
        }
    }

    $scope.retornarIdentificacaoPedidoAnterior = function (pedido) {
        var index = $scope.PedidosAnteriores.indexOf(pedido) + 1;
        return i18next.t('pedido.itmPreviousOrders', {
            numero: index,
            lanches: pedido.lanches.length,
            pessoas: pedido.quantidadePessoas
        });
    }

    $scope.retornarIdentificacaoLanche = function (pedido) {
        var index = $scope.pedido.lanches.indexOf(pedido) + 1;

        var itens = [];

        if (pedido.pao) {
            itens.push($scope.retornarNome(pedido.pao));
        }

        if (pedido.queijo) {
            itens.push($scope.retornarNome(pedido.queijo));
        }

        if (pedido.recheio) {
            itens.push($scope.retornarNome(pedido.recheio));
        }

        if (pedido.molho) {
            for (var x = 0; x < pedido.molho.length; x++) {
                itens.push($scope.retornarNome(pedido.molho[x]));
            }
        }

        if (pedido.tempero) {
            for (var x = 0; x < pedido.tempero.length; x++) {
                itens.push($scope.retornarNome(pedido.tempero[x]));
            }
        }

        return index + " - " + itens.join(" - ")

        //return $i18next.t('pedido.itmSnackNumber', {
        //    numero: ($scope.pedido.lanches.indexOf(pedido) + 1)
        //});
    }

    $scope.Paes = [
        {
            id: 1
            , nome: 'pedido.itmWhite'
            , valor: 1.00
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmWheat'
            , valor: 1.10
        }
        ,
        {
            id: 3
            , nome: 'pedido.itmFrench'
            , valor: 1.50
        }
    ];

    $scope.Queijos = [
        null
        ,
        {
            id: 1
            , nome: 'pedido.itmCheddar'
            , valor: 0.50
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmMozzarella'
            , valor: 0.80
        }
    ];

    $scope.Recheios = [
        null
        ,
        {
            id: 1
            , nome: 'pedido.itmChicken'
            , valor: 3.00
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmRoastBeef'
            , valor: 4.50
        }
        ,
        {
            id: 3
            , nome: 'pedido.itmTurkeyBreast'
            , valor: 6.50
        }
    ];

    $scope.Saladas = [
        {
            id: 1
            , nome: 'pedido.itmLettuce'
            , valor: 1.00
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmArugula'
            , valor: 0.85
        }
        ,
        {
            id: 3
            , nome: 'pedido.itmChard'
            , valor: 1.10
        }
    ];

    $scope.Molhos = [
        {
            id: 1
            , nome: 'pedido.itmItalian'
            , valor: 0.90
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmSpicy'
            , valor: 0.60
        }
    ];

    $scope.Temperos = [
        {
            id: 1
            , nome: 'pedido.itmChili'
            , valor: 1.60
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmSalt'
            , valor: 0.45
        }
        ,
        {
            id: 3
            , nome: 'pedido.itmOregano'
            , valor: 0.90
        }
    ];

    $scope.ListaDeLanchesProntos = [
        {
            id: 0
            , nome: ''
            , pao: $scope.Paes[0]
            , queijo: null
            , recheio: null
            , salada: $scope.Saladas[0]
            , molho: []
            , tempero: []
        }
        ,
        {
            id: 1
            , nome: 'pedido.itmMcChicken'
            , pao: $scope.Paes[0]
            , queijo: $scope.Queijos[1]
            , recheio: $scope.Recheios[1]
            , salada: $scope.Saladas[0]
            , molho: [
                $scope.Molhos[1]
            ]
            , tempero: [
                $scope.Temperos[2]
            ]
        }
        ,
        {
            id: 2
            , nome: 'pedido.itmMcGrill'
            , pao: $scope.Paes[1]
            , queijo: $scope.Queijos[1]
            , recheio: $scope.Recheios[2]
            , salada: $scope.Saladas[1]
            , molho: [
                $scope.Molhos[0]
            ]
            , tempero: [
                $scope.Temperos[0]
            ]
        }
        ,
        {
            id: 3
            , nome: 'pedido.itmMcTurkey'
            , pao: $scope.Paes[2]
            , queijo: $scope.Queijos[2]
            , recheio: $scope.Recheios[3]
            , salada: $scope.Saladas[2]
            , molho: [
                $scope.Molhos[1]
            ]
            , tempero: [
                $scope.Temperos[1]
            ]
        }
    ];
    $scope.lancheProntoSelecionado = $scope.ListaDeLanchesProntos[0];

    $scope.saladaSelecionada = function (idSalada) {
        return (lanche.salada.id == idSalada);
    }

    $scope.setarLanche = function (lanche) {
        $scope.lanche = JSON.parse(JSON.stringify(lanche));
        $scope.lanche.nome = undefined;
        $scope.lanche.id = undefined;
    }

    $scope.salvarLanche = function () {
        $scope.pedido.lanches.push($scope.lanche);
        $scope.novoLanche();
    }

    $scope.novoLanche = function () {
        $scope.lancheProntoSelecionado = $scope.ListaDeLanchesProntos[0];
        $scope.setarLanche($scope.ListaDeLanchesProntos[0]);
    }

    $scope.novoPedido = function () {
        $scope.pedido = {};

        $scope.pedido.tipoPagamento = 1
        $scope.pedido.troco = false;
        $scope.pedido.quantidadePessoas = null;
        $scope.pedido.email = login.email;

        $scope.pedido.nomeCliente = login.nome;
        $scope.pedido.endereco = login.endereco;

        $scope.pedido.lanches = [];
        $scope.pessoas = [];

        $scope.novoLanche();
        $scope.atualizarPedidosAnteriores();
    };

    $scope.finalizarPedido = function () {

        if (!$scope.pedido.nomeCliente || $scope.pedido.nomeCliente == "") {
            toastr["error"](i18next.t("mensagem.MSG01", { campo: i18next.t("pedido.lblName") }));
            return;
        }

        if (!$scope.pedido.endereco || $scope.pedido.endereco == "") {
            toastr["error"](i18next.t("mensagem.MSG01", { campo: i18next.t("pedido.lblAddress") }));
            return;
        }

        if ($scope.pedido.lanches.length <= 0) {
            toastr["error"](i18next.t("mensagem.MSG02"));
            return;
        }

        $('#pagamento').modal();

    }

    $scope.gravarPedido = function () {

        var urlMetodo = hostService + "pedido/inserir";

        $.ajax({
            method: "POST",
            url: urlMetodo,
            data: JSON.stringify($scope.pedido),
            contentType: "application/json",
        })
        .done(function (msg) {
            $('#pagamento').modal('hide');

            $scope.novoPedido();;
        }).fail(function (err) {
            toastr["error"](i18next.t("mensagem.MSG04"));
        });
    }

    $scope.valorLanche = function (lanche) {
        var retorno = 0.0;

        if (!lanche) {
            return 0.0;
        }

        if (lanche.pao) retorno += lanche.pao.valor;
        if (lanche.queijo) {
            retorno += lanche.queijo.valor;
            if (lanche.dobroQueijo) retorno += lanche.queijo.valor;
        }

        if (lanche.recheio) {
            retorno += lanche.recheio.valor;
            if (lanche.dobroRecheio) retorno += lanche.recheio.valor;
        }
        if (lanche.salada) {
            retorno += lanche.salada.valor;
            if (lanche.dobroSalada) retorno += lanche.salada.valor;
        }
        if (lanche.molho) {
            for (var y = 0; y < lanche.molho.length; y++) {
                retorno += lanche.molho[y].valor;
            }
        }
        if (lanche.tempero) {
            for (var y = 0; y < lanche.tempero.length; y++) {
                retorno += lanche.tempero[y].valor;
            }
        }

        return retorno;
    }

    $scope.valorPedido = function () {
        var retorno = 0.0;

        for (var x = 0; x < $scope.pedido.lanches.length; x++) {
            retorno += $scope.valorLanche($scope.pedido.lanches[x])
        }

        return retorno;
    }

    $scope.valorPorPessoa = function () {

        if ($scope.pedido.quantidadePessoas && $scope.pedido.quantidadePessoas > 0) {
            return ($scope.valorPedido() / $scope.pedido.quantidadePessoas);
        } else {
            return 0.0
        }
    }

    $scope.adicionarLanche = function () {
        $scope.ListaDeLanches.push($scope.lanche);
        $scope.novoLanche();
    }

    $scope.novoPedido();

    $(".container").show();
});

window.i18next
    .use(window.i18nextXHRBackend);

window.i18next.init({
    //debug: true,
    lng: 'pt',
    fallbackLng: 'pt',
    backend: {
        loadPath: 'locales/{{lng}}/{{ns}}.json'
    },
    crossDomain: true,
    //withCredentials: true,
    //useCookie: false,
    //useLocalStorage: false
}, function (err, t) {
    $("[ng-controller]").scope().$apply();
});

function setIdiomaIngles() {
    i18next.changeLanguage('en', function () {
        $("[ng-controller]").scope().$applyAsync();
    });

    return false;
}

function setIdiomaPortugues() {
    i18next.changeLanguage('pt', function () {
        $("[ng-controller]").scope().$applyAsync();
    });

    return false;
}