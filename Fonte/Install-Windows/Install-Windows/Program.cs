﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Install_Windows
{
    class Program
    {
        private static string pathAplicacao
        {
            get
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }

        static void Main(string[] args)
        {
            //// Criar aplicativo no IIS
            //// CriarAplicativo();

            //// Iniciar serviço do TomCat
            IniciarServico();

            //// Iniciar página
            AbrirPagina();

            //// Aguardar 20 segundos com o console aberto
            Thread.Sleep(20000);
        }

        private static void CriarAplicativo()
        {
            Console.WriteLine("Criando aplicativo no IIS");

            try
            {
                //// TODO: Mudar o caminho do projeto
                string pathProjeto = @"D:\Workspace\Entelgy\Fonte\Frontend\Entegy";
                string command1 = @"cd C:\Windows\syswow64\inetsrv\";
                string command2 = @"appcmd add app /site.name:""Default Web Site"" /path:/Entelgy.App /physicalPath:" + pathProjeto;

                Process process = new Process();
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C " + command1 + command2;
                process.StartInfo = startInfo;
                process.Start();
            } catch
            {
                Console.WriteLine("FALHA: Esse passo exige que o IIS esteja instalado");
            }


        }

        private static void AbrirPagina()
        {
            Console.WriteLine("Abrindo página");
            Process.Start("chrome.exe", "http://localhost/Entelgy.app/");
            Console.WriteLine("OK - Página aberta");
        }

        private static void IniciarServico()
        {
            Console.WriteLine("Iniciando serviço rest");

            const string pathStartup = @"\..\..\Utils\Tomcat-8\bin\";

            //// Pegar diretório com base no atual desse executável
            string path = string.Format(@"{0}{1}",
                pathAplicacao,
                pathStartup);

            string batDir = path;
            Process proc = new Process();
            proc.StartInfo.WorkingDirectory = batDir;
            proc.StartInfo.FileName = "startup.bat";
            proc.StartInfo.CreateNoWindow = false;
            proc.Start();
            proc.WaitForExit();

            Console.WriteLine("OK - Iniciou serviço rest");
        }
    }
}
