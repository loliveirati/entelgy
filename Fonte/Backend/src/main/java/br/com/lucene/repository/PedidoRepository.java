package br.com.lucene.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.BsonDocument;
import org.bson.Document;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;

import br.com.lucene.model.Login;
import br.com.lucene.model.Pedido;

public class PedidoRepository {
	private final String _collectionDb = "pedido";
	
	
	public void inserir(Pedido pedido) {
		/*Document document = new Document("", new Gson().toJson(pedido));*/ 
		
		MongoSingleton
			.getInstance()
			.getDb().getCollection(_collectionDb)
			.insertOne(Document.parse(new Gson().toJson(pedido)));
	}
	
	public List<Pedido> getPedidos(String email) {
		List<Pedido> ret = null;
		
		BasicDBObject filter = new BasicDBObject();
		
		filter.put("email", email);
		
		FindIterable<Document> col = MongoSingleton
			.getInstance()
			.getDb().getCollection(_collectionDb)
			.find(filter);
		
		if (col == null)
			return null;
		
		ret = new ArrayList<Pedido>();
		
		for (Document document : col) {
            ret.add(new Gson().fromJson(document.toJson(), Pedido.class));
		}
		
		return ret;
	}
}
