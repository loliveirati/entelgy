package br.com.lucene.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pedido {
	private String email;
	private String nomeCliente;
	private String endereco;
	private int tipoPagamento;
	private boolean troco;
	private ArrayList<Lanches> lanches;
	private int quantidadePessoas;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public int getTipoPagamento() {
		return tipoPagamento;
	}
	public void setTipoPagamento(int tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	public boolean isTroco() {
		return troco;
	}
	public void setTroco(boolean troco) {
		this.troco = troco;
	}	
	public ArrayList<Lanches> getLanches() {
		return lanches;
	}
	public void setLanches(ArrayList<Lanches> lanches) {
		this.lanches = lanches;
	}
	public int getQuantidadePessoas() {
		return quantidadePessoas;
	}
	public void setQuantidadePessoas(int quantidadePessoas) {
		this.quantidadePessoas = quantidadePessoas;
	}
}
