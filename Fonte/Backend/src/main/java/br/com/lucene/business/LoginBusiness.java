package br.com.lucene.business;

import br.com.lucene.model.Login;
import br.com.lucene.repository.LoginRepository;

public class LoginBusiness {
	public Login get(String email, String senha) throws Exception {
		if (email.isEmpty())
			throw new Exception("Email inválido!");
		
		if (senha.isEmpty())
			throw new Exception("Senha inválida!");
		
		try {
			return new LoginRepository().get(email, senha);
		} catch (Exception ex){
			throw ex;
		}
	}
}
