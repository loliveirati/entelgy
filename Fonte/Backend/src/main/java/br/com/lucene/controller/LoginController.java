package br.com.lucene.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.lucene.business.LoginBusiness;


@Path("/login")
public class LoginController {
	
	@GET
	@Path("/{email}/{senha}")
	@Produces("application/json")
	public Response get (@PathParam("email") String email,
					     @PathParam("senha") String senha) {
		
		try {
			return Response.ok(new LoginBusiness().get(email, senha))
					.header("Access-Control-Allow-Origin", "*")
				    .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
					.build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.header("Access-Control-Allow-Origin", "*")
				    .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
					.build();
		}
	}
}
