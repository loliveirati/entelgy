package br.com.lucene.model;

import java.util.ArrayList;

public class Lanches {
	private Item pao;
	private Item queijo;
	private Item recheio;
	private Item salada;
	private ArrayList<Item> molho;
	private ArrayList<Item> tempero;
	
	public Item getPao() {
		return pao;
	}
	public void setPao(Item pao) {
		this.pao = pao;
	}
	public Item getQueijo() {
		return queijo;
	}
	public void setQueijo(Item queijo) {
		this.queijo = queijo;
	}
	public Item getRecheio() {
		return recheio;
	}
	public void setRecheio(Item recheio) {
		this.recheio = recheio;
	}
	public Item getSalada() {
		return salada;
	}
	public void setSalada(Item salada) {
		this.salada = salada;
	}
	public ArrayList<Item> getMolho() {
		return molho;
	}
	public void setMolho(ArrayList<Item> molho) {
		this.molho = molho;
	}
	public ArrayList<Item> getTempero() {
		return tempero;
	}
	public void setTempero(ArrayList<Item> tempero) {
		this.tempero = tempero;
	}
}
