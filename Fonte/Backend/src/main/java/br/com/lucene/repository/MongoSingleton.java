package br.com.lucene.repository;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;


public class MongoSingleton {
	private static MongoSingleton mMongoSingleton;
	
	private static MongoClient mongoClient;
	private static MongoDatabase db ;
	
	// TODO: Modificar para parametrizado em .config
	private static final String dbName = "entelgy";
	private static final String dbUri = "mongodb://admin:ENtelgy@ds139278.mlab.com:39278/entelgy";
	
	private MongoSingleton(){};
	
	public static MongoSingleton getInstance(){
		if(mMongoSingleton == null){
			mMongoSingleton = new MongoSingleton();
		}
		return mMongoSingleton;
	} 
	
	public MongoDatabase getDb(){
		if(mongoClient == null){
			MongoClientURI uri = new MongoClientURI(dbUri);
			
			mongoClient = new MongoClient(uri);
		}
		
		if(db == null)
			db = mongoClient.getDatabase(dbName);
		
		return db;
	}
}
