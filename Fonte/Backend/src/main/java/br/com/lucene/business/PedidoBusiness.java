package br.com.lucene.business;

import java.util.List;

import br.com.lucene.model.Pedido;
import br.com.lucene.repository.PedidoRepository;

public class PedidoBusiness {
	public void inserir(Pedido pedido) throws Exception {
		if (pedido == null)
			throw new Exception("Pedido inválido para gravação!");
		
		try {
			new PedidoRepository().inserir(pedido);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public List<Pedido> getPedidos(String email) throws Exception {
		if (email.isEmpty())
			throw new Exception("Email inválido!");
		
		try {
			return new PedidoRepository().getPedidos(email);
		} catch (Exception e) {
			throw e;
		}
	}
}
