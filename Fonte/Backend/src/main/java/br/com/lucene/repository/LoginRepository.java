package br.com.lucene.repository;

import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;

import br.com.lucene.model.Login;

public class LoginRepository {
	private final String _collectionDb = "login";
	
	public Login get(String email, String senha) {
		Login ret = null;
		
		BasicDBObject filter = new BasicDBObject();
		
		filter.put("email", email);
		filter.put("senha", senha);
		
		Document doc = MongoSingleton
			.getInstance()
			.getDb().getCollection(_collectionDb)
			.find(filter).first();
		
		if (doc == null)
			return null;
		
		ret = new Gson().fromJson(doc.toJson(), Login.class);
		
		return ret;
	}
}
