package br.com.lucene.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;

import br.com.lucene.business.PedidoBusiness;
import br.com.lucene.model.Login;
import br.com.lucene.model.Pedido;

@Path("/pedido")
public class PedidoController {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/inserir")
	public Response inserir(String pedidoJson){
		try {
			if (pedidoJson.isEmpty())
				return Response.status(Status.UNSUPPORTED_MEDIA_TYPE).build();
			
			Pedido pedido = new Gson().fromJson(pedidoJson, Pedido.class);
			
			PedidoBusiness bo = new PedidoBusiness();
			bo.inserir(pedido);
			
			return Response.ok()
					.header("Access-Control-Allow-Origin", "*")
				    .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
					.build();
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.header("Access-Control-Allow-Origin", "*")
				    .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
				    .build();
		}
	}
	
	@GET
	@Produces("application/json")
	@Path("/{email}")
	public Response get(@PathParam("email") String email) {
		try {			
			return Response.ok(new Gson()
					.toJson(new PedidoBusiness().getPedidos(email)))
					.header("Access-Control-Allow-Origin", "*")
				    .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
					.build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.header("Access-Control-Allow-Origin", "*")
				    .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
					.build();
		}
	}
}
